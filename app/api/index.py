import logging

import pydantic
from fastapi import APIRouter


router = APIRouter(tags=['home'])


logger = logging.getLogger(__name__)


class ApiIndexResponse(pydantic.BaseModel):
    message: str


@router.get('/', response_model=ApiIndexResponse)
def index() -> ApiIndexResponse:
    logger.info('Printing welcome')
    return ApiIndexResponse(message='Welcome to Translation Challenge API')
