import logging
import uuid
from math import ceil
from typing import Annotated

import pydantic
from fastapi import APIRouter, Depends, HTTPException, Path, Query
from fastapi_cache.decorator import cache
from fastapi_filter import FilterDepends
from fastapi_filter.contrib.sqlalchemy import Filter
from sqlalchemy.orm import Query as SQLAlchemyQuery
from sqlalchemy.orm import Session, joinedload, noload

from app import schemas, settings
from app.database import Definition, Example, Status, Text, get_db_session
from app.google_data_fetcher import FetchedData, fetch_data_from_google

router = APIRouter(prefix='/texts', tags=['texts'])


logger = logging.getLogger(__name__)


class ApiTextResponse(pydantic.BaseModel):
    text_value: str
    language: settings.LanguageEnum | str = settings.LanguageEnum.AUTO

    definitions: list[schemas.DefinitionSchema] | None
    examples: list[schemas.ExampleSchema] | None
    translations: list[schemas.TranslationSchema] | None
    synonyms: list[schemas.SynonymSchema] | None

    class Config:
        orm_mode = True


class TextListFilter(Filter):
    text_value: str | None
    text_value__neq: str | None
    text_value__in: list[str] | None
    text_value__not_in: list[str] | None
    text_value__like: str | None
    text_value__ilike: str | None

    language: str | None
    language__neq: str | None
    language__in: list[str] | None
    language__not_in: list[str] | None

    status: str | None
    status__neq: str | None
    status__in: list[str] | None
    status__not_in: list[str] | None

    class Constants(Filter.Constants):
        model = Text


class TextListIncludeToResultFilter(Filter):
    include_to_result__in: list[str] | None

    @pydantic.validator('include_to_result__in')
    def restrict_filtering_fields(cls, value):
        if value is None:
            return []

        included_categories = {category.lower() for category in value or []}

        invalid_categories = included_categories - cls.Constants.allowed_categories
        if invalid_categories:
            raise ValueError(
                f'Wrong passed include to result categories {list(invalid_categories)}. '
                f'Only {list(cls.Constants.allowed_categories)} are allowed '
            )

        return value

    def filter(self, query: SQLAlchemyQuery) -> SQLAlchemyQuery:
        categories = set(self.include_to_result__in or [])
        for category in self.Constants.allowed_categories:
            if category in categories:
                query = query.options(joinedload(getattr(Text, category)))
            else:
                query = query.options(noload(getattr(Text, category)))
        return query

    def sort(self, query: SQLAlchemyQuery):
        return query

    class Constants(Filter.Constants):
        allowed_categories = {
            'definitions',
            'examples',
            'synonyms',
            'translations',
        }


class TextListSort(Filter):
    order_by: list[str] | None

    @pydantic.validator('order_by')
    def restrict_sortable_fields(cls, value):
        if value is None:
            return None

        allowed_field_names = {col.name for col in Text.__table__.columns}

        for field_name in value:
            field_name = field_name.replace('+', '').replace('-', '')
            if field_name not in allowed_field_names:
                raise ValueError(f"You may only sort by: {', '.join(allowed_field_names)}")

        return value

    class Constants(Filter.Constants):
        model = Text


class ApiTextListResponse(pydantic.BaseModel):
    count: int
    page: int
    page_size: int
    total_pages: int
    result: list[ApiTextResponse]


@router.get('/', response_model=ApiTextListResponse)
def get_text_list(
    text_filter: TextListFilter = FilterDepends(TextListFilter),
    include_to_result_filter: TextListIncludeToResultFilter = FilterDepends(TextListIncludeToResultFilter),
    order_by: TextListSort = FilterDepends(TextListSort),
    page: Annotated[int, Query(ge=1)] = 1,
    page_size: Annotated[int, Query(ge=1, le=100)] = 50,
    db_session: Session = Depends(get_db_session),
) -> ApiTextListResponse:
    query = db_session.query(Text)

    query = text_filter.filter(query)
    total = query.count()

    query = include_to_result_filter.filter(query)

    query = order_by.sort(query)

    items = query.offset((page - 1) * page_size).limit(page_size)
    total_pages = ceil(total / page_size)

    return ApiTextListResponse(
        page=page,
        page_size=page_size,
        count=total,
        total_pages=total_pages,
        result=items.all(),
    )


@router.get('/{text}', response_model=ApiTextResponse)
@cache(expire=settings.CACHE_EXPIRE_PERIOD)
def get_text(
    text: Annotated[str, Path(min_length=1, max_length=settings.MAX_TEXT_LENGTH_ALLOWED)],
    source_lang: settings.LanguageEnum,
    target_lang: settings.LanguageEnum,
    db_session: Session = Depends(get_db_session),
) -> ApiTextResponse:
    """
    There are 3 possible scenarios:
    1. The given text exists in the database so its data are just fetched and returned to a user.
    2. The given text does not exist in the database so the Google Translate API is requested to fill the data,
       and return them after that.
    3. The given text exists in the database, but has a status "need update". That means the text has been added to
       the database before but as a part of synonym/translation of another text.
       In that case, the Google Translate API is requested again to fetch missing data.

    In p.2 and 3 there might be a case when translations or synonyms already exist in the database
    (because they just other texts, and might be added before). So, here they should be also linked to the given text.
    """

    logger.info(f'Handling text "{text}"')
    text_value = text.lower()
    db_text: Text = (
        db_session.query(Text)
        .options(joinedload(Text.translations))
        .options(joinedload(Text.synonyms))
        .options(joinedload(Text.examples))
        .options(joinedload(Text.definitions))
        .filter(Text.text_value == text, Text.language == source_lang, Text.deleted == False)
        .one_or_none()
    )
    if db_text is None or db_text.status != Status.DONE:
        logger.info(f'Need to update data')
        if db_text is None:
            logger.info(f'Detecting new text')
            db_text = Text(
                id=uuid.uuid4(),
                text_value=text_value,
                language=source_lang,
            )

        try:
            logger.info('Requesting to Google API for text details')
            data = fetch_data_from_google(
                text=text_value,
                source_lang=source_lang,
                target_lang=target_lang,
            )
        except Exception as e:
            logger.exception(f'Failed to fetch data from Google Translate API. Reason: {e}')
            raise HTTPException(status_code=500, detail='Internal server error')

        db_text.status = Status.DONE
        db_session.add(db_text)

        try:
            logger.info(f'Updating data for "{db_text.text_value}"')
            _update_data(db_session=db_session, text=db_text, new_data=data)
        except Exception as e:
            logger.exception(f'Failed to update data for {text}. Reason: {e}')
            raise HTTPException(status_code=500, detail='Internal server error')
    else:
        logger.info('Taking data from db')

    if target_lang:
        logger.info(f'Extracting only translations for lang {target_lang}')
        db_text = (
            db_session.query(Text)
            .options(joinedload(Text.translations))
            .options(joinedload(Text.synonyms))
            .options(joinedload(Text.examples))
            .options(joinedload(Text.definitions))
            .filter(Text.text_value == text, Text.language == source_lang, Text.deleted == False)
            .one_or_none()
        )
        db_session.expunge(db_text)
        db_text.translations = [
            translated_text for translated_text in db_text.translations if translated_text.language == target_lang
        ]

    return ApiTextResponse(
        text_value=db_text.text_value,
        language=db_text.language,
        definitions=db_text.definitions,
        examples=db_text.examples,
        translations=list(map(schemas.TranslationSchema.from_orm, db_text.translations)),
        synonyms=list(map(schemas.SynonymSchema.from_orm, db_text.synonyms)),
    )


@router.delete('/{text}', response_model=str)
def delete_text(text: str, db_session: Session = Depends(get_db_session)) -> str:
    db_texts: list[Text] = (
        db_session.query(Text)
        .options(joinedload(Text.translations))
        .options(joinedload(Text.synonyms))
        .options(joinedload(Text.examples))
        .options(joinedload(Text.definitions))
        .filter(Text.text_value == text)
        .all()
    )
    if db_texts:
        logger.info('Detecting data to delete')
        for db_text in db_texts:
            db_text.deleted = True
            db_session.add(db_text)

        db_session.commit()
    return 'deleted'


def _update_data(
    db_session: Session,
    text: Text,
    new_data: FetchedData,
) -> None:
    translation_values_translations: dict[str, schemas.TranslationSchema] = {
        translation.text_value: translation for translation in new_data.translations
    }
    synonym_values_synonyms: dict[str, schemas.SynonymSchema] = {
        synonym.text_value: synonym for synonym in new_data.synonyms
    }

    texts = list(translation_values_translations) + list(synonym_values_synonyms)
    db_existing_text_values_texts: dict[str, Text] = {
        db_text_item.text_value: db_text_item for db_text_item in _get_text_data(db_session, texts)
    }

    new_translations = [
        Text(id=uuid.uuid4(), **item.dict())
        for text_value, item in translation_values_translations.items()
        if text_value not in db_existing_text_values_texts
    ]
    if new_translations:
        logger.info('Adding new translations')
        text.translations.extend(new_translations)

    existing_translations = [
        db_existing_text_values_texts[text_value]
        for text_value, item in translation_values_translations.items()
        if text_value in db_existing_text_values_texts
    ]
    if existing_translations:
        logger.info('Linking existing translations')
        text.translations.extend(existing_translations)

    new_synonyms = [
        Text(id=uuid.uuid4(), **item.dict())
        for text_value, item in synonym_values_synonyms.items()
        if text_value not in db_existing_text_values_texts
    ]
    if new_synonyms:
        logger.info('Adding new synonyms')
        text.synonyms.extend(new_synonyms)

    existing_synonyms = [
        db_existing_text_values_texts[text_value]
        for text_value, item in synonym_values_synonyms.items()
        if text_value in db_existing_text_values_texts
    ]
    if existing_synonyms:
        logger.info('Linking existing synonyms')
        text.synonyms.extend(existing_synonyms)

    if new_data.examples:
        logger.info('Adding new examples')
        text.examples.extend(Example(id=uuid.uuid4(), **item.dict()) for item in new_data.examples)

    if new_data.definitions:
        logger.info('Adding new definitions')
        text.definitions.extend(Definition(id=uuid.uuid4(), **item.dict()) for item in new_data.definitions)

    logger.info('Commiting to db')
    db_session.commit()


def _get_text_data(db_session: Session, texts: list[str]) -> list[Text]:
    return db_session.query(Text).filter(Text.text_value.in_(texts)).all()  # type: ignore
