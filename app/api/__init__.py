from fastapi import APIRouter


def get_v1_routers() -> list[APIRouter]:
    from app.api.index import router as index_router_v1
    from app.api.texts import router as text_router_v1

    return [index_router_v1, text_router_v1]
