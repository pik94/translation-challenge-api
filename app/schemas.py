import pydantic

from app import settings


class TextSchema(pydantic.BaseModel):
    text_value: str
    language: settings.LanguageEnum = settings.LanguageEnum.AUTO


class DefinitionSchema(pydantic.BaseModel):
    text_value: str
    kind: str

    class Config:
        orm_mode = True


class ExampleSchema(pydantic.BaseModel):
    text_value: str
    kind: str

    class Config:
        orm_mode = True


class SynonymSchema(TextSchema):
    class Config:
        orm_mode = True


class TranslationSchema(TextSchema):
    class Config:
        orm_mode = True
