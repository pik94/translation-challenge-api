def parse_boolean(value: str) -> bool:
    true = {'yes', 'true', 'on', '1'}
    false = {'no', 'false', 'off', '0', 'none'}
    value = value.strip().lower()
    if value in true:
        return True
    elif value in false:
        return False
    else:
        raise ValueError(f'Invalid boolean value {value!r}')
