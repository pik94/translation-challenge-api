import logging

from app.database.base import engine

logger = logging.getLogger(__name__)


def post_fork(server, worker):
    logger.info("Afterfork cleanup")
    engine.dispose()
