from contextvars import ContextVar
import logging
import time
import uuid

from fastapi import FastAPI, Request, Response
from fastapi.middleware.cors import CORSMiddleware
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend
import aioredis

from app import settings


request_id_context: ContextVar[str | None] = ContextVar('request_id', default=None)


def setup_logging():
    def format_log() -> str:
        fields = (
            "asctime",
            "levelname",
            "filename",
            "funcName",
            "message",
        )

        return ' | '.join(f'%({i:s})s' for i in fields)

    class CustomFormatter(logging.Formatter):
        def format(self, record: logging.LogRecord):
            record.msg = f'request_id = {request_id_context.get()} | {record.msg}'
            return super().format(record)

    formatter = CustomFormatter(format_log())
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logging.root.addHandler(handler)


def create_app():
    from app.api import get_v1_routers

    setup_logging()

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    app = FastAPI(title='Google translator server')
    for router in get_v1_routers():
        app.include_router(router=router)

    app.add_middleware(
        CORSMiddleware,
        allow_origins=['*'],
        allow_credentials=True,
        allow_methods=['*'],
        allow_headers=['*'],
    )

    @app.middleware('http')
    async def log_request(request: Request, call_next):
        start = time.monotonic()
        request_id = str(uuid.uuid4())
        request_id_context.set(request_id)

        try:
            logger.info('Request started')
            response: Response = await call_next(request)
            response.headers.append('request-id', request_id)
            return response

        finally:
            logger.info(f'Request ended. Elapsed time: {time.monotonic() - start}')

    @app.on_event('startup')
    async def startup():
        redis = aioredis.from_url(f'redis://{settings.REDIS_HOST}:{settings.REDIS_PORT}')
        FastAPICache.init(RedisBackend(redis), prefix='translation-api-cache')

    return app
