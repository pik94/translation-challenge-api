import logging

import httpx
import pydantic

from app import schemas, settings

logger = logging.getLogger(__name__)


def _extract_translations(
    data: list[dict],
    lang: settings.LanguageEnum,
) -> list[schemas.TranslationSchema]:
    return [
        schemas.TranslationSchema(
            text_value=item['trans'],
            language=lang,
        )
        for item in data
        if 'trans' in item
    ]


def _extract_definitions(data: list[dict]) -> list[schemas.DefinitionSchema]:
    return [
        schemas.DefinitionSchema(
            text_value=definition['gloss'],
            kind=item['pos'],
        )
        for item in data
        for definition in item.get('entry', [])
        if 'example' in definition
    ]


def _extract_synonyms(
    data: list[dict],
    lang: settings.LanguageEnum,
) -> list[schemas.SynonymSchema]:
    return [
        schemas.SynonymSchema(
            text_value=synonym,
            language=lang,
        )
        for item in data
        for entry_list in item.get('entry', [])
        for synonym in entry_list.get('synonym', [])
    ]


def _extract_examples(data: list[dict]) -> list[schemas.ExampleSchema]:
    return [
        schemas.ExampleSchema(
            text_value=definition['example'],
            kind=item['pos'],
        )
        for item in data
        for definition in item.get('entry', [])
        if 'example' in definition
    ]


class FetchedData(pydantic.BaseModel):
    translations: list[schemas.TranslationSchema]
    definitions: list[schemas.DefinitionSchema]
    synonyms: list[schemas.SynonymSchema]
    examples: list[schemas.ExampleSchema]


def fetch_data_from_google(
    text: str,
    source_lang: settings.LanguageEnum,
    target_lang: settings.LanguageEnum,
) -> FetchedData:
    params = {
        'q': text,
        'sl': source_lang,
        'tl': target_lang,
        'client': 'gtx',
        'dt': ['t', 'bd', 'md', 'ss', 'ex'],
        'dj': 1,
    }

    url = 'https://translate.googleapis.com/translate_a/single'
    client = httpx.Client()
    try:
        request = client.build_request(method='GET', url=url, params=params)
        logger.info(f'Sending a request to Google API: {request.url}')
        rv = client.send(request=request)
    finally:
        client.close()

    if rv.status_code != 200:
        raise ValueError(f'Request to {url} failed with the status code {rv.status_code}. Reason: {rv.text}')

    data = rv.json()
    if 'sentences' not in data:
        raise ValueError('Cannot find "sentences" block in the response')

    sentences = data['sentences']
    if not sentences:
        raise ValueError('Empty sentence block, cannot extract translation')

    translations = _extract_translations(data=sentences, lang=target_lang)
    definitions = _extract_definitions(data=data.get('definitions', []))
    synonyms = _extract_synonyms(data=data.get('synsets', []), lang=source_lang)
    examples = _extract_examples(data=data.get('definitions', []))

    return FetchedData(
        translations=translations,
        definitions=definitions,
        synonyms=synonyms,
        examples=examples,
    )
