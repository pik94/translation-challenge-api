import uuid

from sqlalchemy import Boolean, Column, ForeignKey, String, Table, UniqueConstraint
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import relationship
from sqlalchemy_utils import generic_repr

from app.database.base import Base, DbStatus, Status
from app.database.mixins import PrimaryKeyUUIDMixin, TimestampMixin

texts_translations = Table(
    'texts_translations',
    Base.metadata,
    Column(
        'source_text_id',
        postgresql.UUID(as_uuid=True),
        ForeignKey('texts.id'),
        primary_key=True,
    ),
    Column(
        'target_text_id',
        postgresql.UUID(as_uuid=True),
        ForeignKey('texts.id'),
        primary_key=True,
    ),
)


texts_synonyms = Table(
    'texts_synonyms',
    Base.metadata,
    Column(
        'source_text_id',
        postgresql.UUID(as_uuid=True),
        ForeignKey('texts.id'),
        primary_key=True,
    ),
    Column(
        'target_text_id',
        postgresql.UUID(as_uuid=True),
        ForeignKey('texts.id'),
        primary_key=True,
    ),
)


@generic_repr('id', 'text_value', 'language', 'status')
class Text(Base, PrimaryKeyUUIDMixin, TimestampMixin):
    __tablename__ = 'texts'
    __table_args__ = (UniqueConstraint('text_value', 'language'),)

    text_value: str = Column(String, nullable=False, index=True)
    language: str = Column(String, nullable=False, index=True)
    status: Status = Column(DbStatus, nullable=False, default=Status.NEED_UPDATE)
    deleted: bool = Column(Boolean, default=False, server_default='FALSE')

    definitions = relationship('Definition', back_populates='text')
    examples = relationship('Example', back_populates='text')

    translations: list['Text'] = relationship(
        'Text',
        secondary=texts_translations,
        primaryjoin='Text.id == texts_translations.c.source_text_id',
        secondaryjoin='Text.id == texts_translations.c.target_text_id',
        back_populates='translations',
    )

    synonyms: list['Text'] = relationship(
        'Text',
        secondary=texts_synonyms,
        primaryjoin='Text.id == texts_synonyms.c.source_text_id',
        secondaryjoin='Text.id == texts_synonyms.c.target_text_id',
        back_populates='synonyms',
    )

    def __repr__(self):
        return 'Text()'


@generic_repr('id', 'text_value', 'kind', 'text_id')
class Definition(Base, PrimaryKeyUUIDMixin, TimestampMixin):
    __tablename__ = 'definitions'

    text_value: str = Column(String, nullable=False, index=True)
    text_id: uuid.UUID = Column(postgresql.UUID(as_uuid=True), ForeignKey('texts.id'))
    kind: str = Column(String, nullable=True, default=None)

    text = relationship('Text', back_populates='definitions')


@generic_repr('id', 'text_value', 'kind', 'text_id')
class Example(Base, PrimaryKeyUUIDMixin, TimestampMixin):
    __tablename__ = 'examples'

    text_value: str = Column(String, nullable=False, index=True)
    text_id: uuid.UUID = Column(postgresql.UUID(as_uuid=True), ForeignKey('texts.id'))
    kind: str = Column(String, nullable=True, default=None)

    text = relationship('Text', back_populates='examples')
