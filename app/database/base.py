import dataclasses
import enum

from sqlalchemy import Enum, MetaData, create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.pool import QueuePool
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session, sessionmaker

from app import settings


def get_db_url():
    return (
        f'postgresql://{settings.DB_USER}:{settings.DB_PASSWORD}'
        f'@{settings.DB_HOST}:{settings.DB_PORT}/{settings.DB_NAME}'
    )


engine = create_engine(
    url=get_db_url(),
    poolclass=QueuePool,
    echo=settings.DB_ECHO,
    pool_size=10,
    max_overflow=200,
    pool_timeout=0,
)


class Status(enum.Enum):
    DONE = 'done'
    NEED_UPDATE = 'need_update'

    def __str__(self):
        return str(self.value)


def _get_enum_values(enum_cls):
    return [item.value for item in enum_cls]


DbStatus = Enum(Status, name='status', values_callable=_get_enum_values)


@dataclasses.dataclass
class Database:
    engine: Engine

    def __post_init__(self):
        self._metadata = declarative_base()
        self._session_factory = None

    def metadata(self) -> MetaData:
        return self._metadata

    @property
    def session_factory(self):
        if not self._session_factory:
            self._session_factory = sessionmaker(
                autocommit=False, autoflush=True, bind=self.engine, expire_on_commit=False
            )
        return self._session_factory

    def create_all(self):
        self._metadata.metadata.create_all(bind=self.engine)

    def drop_all(self):
        self._metadata.metadata.drop_all(bind=self.engine)


db: Database = Database(engine=engine)


@dataclasses.dataclass
class UnscopedSessionContext:
    db: Database

    def __post_init__(self):
        self._session_factory = db.session_factory

    def __enter__(self):
        self._session = self._session_factory()
        return self._session

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._session:
            self._session.close()


def get_db_session() -> Session:
    db_session = db.session_factory()
    try:
        yield db_session
    finally:
        db_session.close()


Base: MetaData = db.metadata()
