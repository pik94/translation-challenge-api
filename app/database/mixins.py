import datetime as dt
import uuid

from sqlalchemy import Column, DateTime
from sqlalchemy.dialects import postgresql


class PrimaryKeyUUIDMixin:
    id: uuid.UUID = Column(postgresql.UUID(as_uuid=True), primary_key=True)


class TimestampMixin:
    updated_at = Column(DateTime(True), default=dt.datetime.utcnow, nullable=False)
    created_at = Column(DateTime(True), default=dt.datetime.utcnow, nullable=False)
