all: build-dev

.PHONY: build-dev
build-dev:
	docker-compose -f dev.yml build


.PHONY: build-prod
build-prod:
	docker-compose -f prod.yml build

.PHONY: run-dev-stack
run-dev-stack:
	mkdir -p data/{postgres,redis}
	docker-compose -f dev.yml up -d --force-recreate


.PHONY: stop-dev-stack
stop-dev-stack:
	docker-compose -f dev.yml down


.PHONY: run-dev-rebuild
run-dev-stack-rebuild:
	mkdir -p data/{postgres,redis}
	docker-compose -f dev.yml up -d --no-deps --build


.PHONY: run-test-stack
run-test-stack:
	docker-compose -f test.yml up -d --force-recreate


.PHONY: stop-test-stack
stop-test-stack:
	docker-compose -f test.yml down


.PHONY: run-tests
run-tests: run-test-stack
	docker build -t translation-challenge-api-test .
	docker container run --rm --network=tc-test-network translation-challenge-api-test tests


.PHONY: run-tests-local
run-tests-local: run-test-stack
	pip install poetry && \
	poetry install --with test && \
	pytest -x


.PHONY: run-checks
run-checks:
	pip install poetry && \
	poetry install --with test && \
	pre-commit run --all-files


clean: stop-dev-stack stop-test-stack
	docker image rm translation-challenge-api-test || true