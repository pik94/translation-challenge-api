FROM python:3.10-slim

WORKDIR /app

ENV MAX_REQUESTS=200 \
    MAX_REQUESTS_JITTER=40 \
    WEB_WORKERS=5 \
    # db creds
    DB_HOST=postgres \
    DB_PORT=5432 \
    DB_USER=postgres \
    DB_PASSWORD=postgres \
    DB_NAME=postgres \
    DB_ECHO=false

COPY pyproject.toml poetry.lock ./

RUN pip install poetry && \
    poetry config virtualenvs.create false && \
    poetry install --no-interaction --no-ansi --only main

EXPOSE 5000

COPY . .

RUN chmod +x docker-entrypoint.sh

ENTRYPOINT ["/app/docker-entrypoint.sh"]
CMD ["server"]