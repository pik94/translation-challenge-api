from app import schemas, settings
from app.google_data_fetcher import _extract_definitions, _extract_examples, _extract_synonyms, _extract_translations


def test_extract_translations():
    data = [{'trans': 'увеличивать', 'orig': 'increase', 'backend': 10}]

    assert _extract_translations(
        data=data,
        lang=settings.LanguageEnum.RU,
    ) == [
        schemas.TranslationSchema(
            text_value='увеличивать',
            language='ru',
        )
    ]


def test_extract_translations_empty():
    data = [{'orig': 'increase', 'backend': 10}]

    assert _extract_translations(data=data, lang=settings.LanguageEnum.RU) == []


def test_extract_definitions():
    data = [
        {
            "pos": "verb",
            "entry": [
                {
                    "gloss": "become or make greater in size, amount, intensity, or degree.",
                    "definition_id": "m_en_gbus0502660.005",
                    "example": "car use is increasing at an alarming rate",
                }
            ],
            "base_form": "increase",
            "pos_enum": 2,
        },
        {
            "pos": "noun",
            "entry": [
                {
                    "gloss": "an instance of growing or making greater.",
                    "definition_id": "m_en_gbus0502660.015",
                    "example": "some increase in inflation",
                }
            ],
            "base_form": "increase",
            "pos_enum": 1,
        },
    ]

    assert _extract_definitions(data=data) == [
        schemas.DefinitionSchema(
            text_value='become or make greater in size, amount, intensity, or degree.',
            kind='verb',
        ),
        schemas.DefinitionSchema(
            text_value='an instance of growing or making greater.',
            kind='noun',
        ),
    ]


def test_extract_definitions_empty():
    data = [
        {
            "pos": "verb",
            "entry": [
                {
                    "definition_id": "m_en_gbus0167500.038",
                },
                {
                    "definition_id": "m_en_gbus0167500.049",
                },
                {"definition_id": "m_en_gbus0167500.053", "label_info": {"subject": ["Medicine"]}},
            ],
            "base_form": "challenge",
            "pos_enum": 2,
        }
    ]
    assert _extract_definitions(data=data) == []


def test_extract_examples():
    data = [
        {
            "pos": "verb",
            "entry": [
                {
                    "gloss": "become or make greater in size, amount, intensity, or degree.",
                    "definition_id": "m_en_gbus0502660.005",
                    "example": "car use is increasing at an alarming rate",
                }
            ],
            "base_form": "increase",
            "pos_enum": 2,
        },
        {
            "pos": "noun",
            "entry": [
                {
                    "gloss": "an instance of growing or making greater.",
                    "definition_id": "m_en_gbus0502660.015",
                    "example": "some increase in inflation",
                }
            ],
            "base_form": "increase",
            "pos_enum": 1,
        },
    ]

    assert _extract_examples(data=data) == [
        schemas.ExampleSchema(text_value="car use is increasing at an alarming rate", kind='verb'),
        schemas.ExampleSchema(
            text_value="some increase in inflation",
            kind='noun',
        ),
    ]


def test_extract_examples_empty():
    data = [
        {
            "pos": "verb",
            "entry": [
                {
                    "gloss": "invite (someone) to engage in a contest.",
                    "definition_id": "m_en_gbus0167500.038",
                },
                {
                    "gloss": "dispute the truth or validity of.",
                    "definition_id": "m_en_gbus0167500.049",
                },
                {
                    "gloss": "expose (the immune system) to pathogenic organisms or antigens.",
                    "definition_id": "m_en_gbus0167500.053",
                    "label_info": {"subject": ["Medicine"]},
                },
            ],
            "base_form": "challenge",
            "pos_enum": 2,
        }
    ]
    assert _extract_examples(data=data) == []


def test_extract_synonyms():
    data = [
        {
            "pos": "verb",
            "entry": [
                {
                    "synonym": [
                        "grow",
                        "get bigger",
                    ],
                    "definition_id": "m_en_gbus0502660.005",
                },
            ],
            "base_form": "increase",
        },
        {
            "pos": "noun",
            "entry": [
                {
                    "synonym": [
                        "growth",
                        "rise",
                    ],
                    "definition_id": "m_en_gbus0502660.015",
                },
            ],
            "base_form": "increase",
        },
    ]

    assert _extract_synonyms(
        data=data,
        lang=settings.LanguageEnum.EN,
    ) == [
        schemas.SynonymSchema(text_value="grow", language="en"),
        schemas.SynonymSchema(text_value="get bigger", language="en"),
        schemas.SynonymSchema(text_value="growth", language="en"),
        schemas.SynonymSchema(text_value="rise", language="en"),
    ]


def test_extract_synonyms_empty():
    data = [
        {
            "pos": "verb",
            "entry": [
                {
                    "definition_id": "m_en_gbus0502660.005",
                },
            ],
            "base_form": "increase",
        },
        {
            "pos": "noun",
            "entry": [
                {
                    "definition_id": "m_en_gbus0502660.015",
                },
            ],
            "base_form": "increase",
        },
    ]

    assert _extract_synonyms(data=data, lang=settings.LanguageEnum.EN) == []
