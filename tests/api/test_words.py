import pytest
from fastapi.testclient import TestClient
from pytest_mock import MockerFixture
from sqlalchemy.orm import Session

from app import schemas
from app.database import Status, Text
from app.google_data_fetcher import FetchedData


@pytest.fixture
def mocked_fetch_data_from_google(mocker: MockerFixture):
    mocker.patch(
        'app.api.texts.fetch_data_from_google',
        return_value=FetchedData(
            translations=[
                schemas.TranslationSchema(text_value='расти', language='ru'),  # type: ignore
            ],
            definitions=[
                schemas.DefinitionSchema(
                    text_value='become larger or greater over a period of time',
                    kind='verb',
                )
            ],
            synonyms=[
                schemas.SynonymSchema(
                    text_value='increase',
                    language='en',  # type: ignore
                ),
                schemas.SynonymSchema(
                    text_value='get taller',
                    language='en',  # type: ignore
                ),
            ],
            examples=[
                schemas.ExampleSchema(
                    text_value='turnover grew to more than $100,000 within three years',
                    kind='verb',
                )
            ],
        ),
    )
    yield


def test_get_text(client: TestClient, populate_words):
    _ = populate_words()
    rv = client.get('/texts/increase?source_lang=en&target_lang=ru')
    assert rv.status_code == 200

    data = rv.json()

    assert 'text_value' in data
    assert 'language' in data
    assert data['text_value'] is not None and len(data['text_value']) > 1
    assert data['language'] == 'en', 'Should be "en", i.e. a source language'

    for item_name in ('definitions', 'examples', 'translations', 'synonyms'):
        assert item_name in data
        assert len(data[item_name]) > 0, f'"{item_name}" should not be empty'

    translations = data['translations']
    languages = {translation['language'] for translation in translations}
    assert len(languages) == 1, 'Should be only one language in translation'
    assert list(languages)[0] == 'ru', 'Should "ru", i.e. target_language'


@pytest.mark.usefixtures('mocked_fetch_data_from_google')
@pytest.mark.usefixtures('clean_database')
def test_get_text_list_new_text_given(client: TestClient, db_session: Session):
    rv = client.get('/texts/grow?source_lang=en&target_lang=ru')
    assert rv.status_code == 200

    data = rv.json()

    assert 'text_value' in data
    assert 'language' in data
    assert data['text_value'] is not None and len(data['text_value']) > 1
    assert data['language'] == 'en', 'Should be "en", i.e. a source language'

    for item_name in ('definitions', 'examples', 'translations', 'synonyms'):
        assert item_name in data
        assert len(data[item_name]) > 0

    translations = data['translations']
    languages = {translation['language'] for translation in translations}
    assert len(languages) == 1, 'Should be only one language in translation'
    assert list(languages)[0] == 'ru', 'Should "ru", i.e. target_language'


@pytest.mark.usefixtures('mocked_fetch_data_from_google')
@pytest.mark.usefixtures('clean_database')
def test_get_text_list_need_to_update_text_given(client, db_session, populate_words):
    """
    A word "grow" and its synonym "increase" exist in the database
    The case expects that "grow" has been as a synonym of "increase" before.
    So, it has a status "need_update", i.e. does not have its own synonyms, definitions and examples in the database
    After requesting of "grow" to the API, data for the "grow" are fetched from Google so its data should not be empty,
    it should have a status "done", and "increase" should steel has a status "done", i.e. not be changed, and should be
    a synonym of the "grow". Also, a word "расти" must be in translations of "grow" as well as of "increase"
    """

    _ = populate_words()  # a word "grow" and its synonym "increase" exist in the database

    rv = client.get('/texts/grow?source_lang=en&target_lang=ru')
    assert rv.status_code == 200

    data = rv.json()

    assert 'text_value' in data
    assert 'language' in data
    assert data['text_value'] is not None and len(data['text_value']) > 1
    assert data['language'] == 'en', 'Should be "en", i.e. a source language'

    for item_name in ('definitions', 'examples', 'translations', 'synonyms'):
        assert item_name in data
        assert len(data[item_name]) > 0

    translations = data['translations']
    languages = {translation['language'] for translation in translations}
    assert len(languages) == 1, 'Should be only one language in translation'
    assert list(languages)[0] == 'ru', 'Should "ru", i.e. target_language'

    assert 'расти' in {item['text_value'] for item in data['translations']}
    assert 'increase' in {item['text_value'] for item in data['synonyms']}

    grow_text: Text = db_session.query(Text).filter(Text.text_value == 'grow').one()
    assert grow_text.status == Status.DONE

    increase_text: Text = db_session.query(Text).filter(Text.text_value == 'increase').one()
    assert increase_text.status == Status.DONE


def test_get_text_list_full(client: TestClient, populate_words):
    _ = populate_words()
    rv = client.get('/texts')
    assert rv.status_code == 200
    data = rv.json()

    assert {
        'count': data['count'],
        'page': data['page'],
        'page_size': data['page_size'],
        'total_pages': data['total_pages'],
        'result_len': len(data['result']),
    } == {
        'count': 16,
        'page': 1,
        'page_size': 50,
        'total_pages': 1,
        'result_len': 16,
    }


@pytest.mark.parametrize(
    'filters, expected_result',
    [
        pytest.param(
            'text_value=increase',
            {
                'count': 1,
                'page': 1,
                'page_size': 50,
                'total_pages': 1,
                'result_len': 1,
            },
            id='text_value',
        ),
        pytest.param(
            'text_value__neq=increase',
            {
                'count': 15,
                'page': 1,
                'page_size': 50,
                'total_pages': 1,
                'result_len': 15,
            },
            id='text_value__neq',
        ),
        pytest.param(
            'text_value__in=increase,grow',
            {
                'count': 2,
                'page': 1,
                'page_size': 50,
                'total_pages': 1,
                'result_len': 2,
            },
            id='text_value__in',
        ),
        pytest.param(
            'text_value__not_in=increase,grow',
            {
                'count': 14,
                'page': 1,
                'page_size': 50,
                'total_pages': 1,
                'result_len': 14,
            },
            id='text_value__not_in',
        ),
        pytest.param(
            'text_value__like=inc',
            {
                'count': 1,
                'page': 1,
                'page_size': 50,
                'total_pages': 1,
                'result_len': 1,
            },
            id='text_value__like',
        ),
        pytest.param(
            'text_value__ilike=inc',
            {
                'count': 1,
                'page': 1,
                'page_size': 50,
                'total_pages': 1,
                'result_len': 1,
            },
            id='text_value__ilike',
        ),
        pytest.param(
            'language=en',
            {
                'count': 8,
                'page': 1,
                'page_size': 50,
                'total_pages': 1,
                'result_len': 8,
            },
            id='language',
        ),
        pytest.param(
            'language__neq=en',
            {
                'count': 8,
                'page': 1,
                'page_size': 50,
                'total_pages': 1,
                'result_len': 8,
            },
            id='language__neq',
        ),
        pytest.param(
            'language__in=en,de',
            {
                'count': 12,
                'page': 1,
                'page_size': 50,
                'total_pages': 1,
                'result_len': 12,
            },
            id='language__in',
        ),
        pytest.param(
            'language__not_in=en,de',
            {
                'count': 4,
                'page': 1,
                'page_size': 50,
                'total_pages': 1,
                'result_len': 4,
            },
            id='language__not_in',
        ),
        pytest.param(
            'page_size=4',
            {
                'count': 16,
                'page': 1,
                'page_size': 4,
                'total_pages': 4,
                'result_len': 4,
            },
            id='page_size_4',
        ),
        pytest.param(
            'page_size=7',
            {
                'count': 16,
                'page': 1,
                'page_size': 7,
                'total_pages': 3,
                'result_len': 7,
            },
            id='page_size_7',
        ),
        pytest.param(
            'page_size=7&page=2',
            {
                'count': 16,
                'page': 2,
                'page_size': 7,
                'total_pages': 3,
                'result_len': 7,
            },
            id='page_size_7_and_page_2',
        ),
        pytest.param(
            'page_size=7&page=4',
            {
                'count': 16,
                'page': 4,
                'page_size': 7,
                'total_pages': 3,
                'result_len': 0,
            },
            id='page_size_7_and_page_4_empty_result',
        ),
        pytest.param(
            'status=done',
            {
                'count': 2,
                'page': 1,
                'page_size': 50,
                'total_pages': 1,
                'result_len': 2,
            },
            id='status',
        ),
        pytest.param(
            'status__neq=done',
            {
                'count': 14,
                'page': 1,
                'page_size': 50,
                'total_pages': 1,
                'result_len': 14,
            },
            id='status__neq',
        ),
        pytest.param(
            'status__in=done',
            {
                'count': 2,
                'page': 1,
                'page_size': 50,
                'total_pages': 1,
                'result_len': 2,
            },
            id='status__in',
        ),
        pytest.param(
            'status__not_in=done',
            {
                'count': 14,
                'page': 1,
                'page_size': 50,
                'total_pages': 1,
                'result_len': 14,
            },
            id='status_not_in',
        ),
    ],
)
def test_get_text_list_text_filter(client: TestClient, populate_words, filters: str, expected_result: dict):
    _ = populate_words()
    rv = client.get(f'/texts?{filters}')

    assert rv.status_code == 200
    data = rv.json()

    assert {
        'count': data['count'],
        'page': data['page'],
        'page_size': data['page_size'],
        'total_pages': data['total_pages'],
        'result_len': len(data['result']),
    } == expected_result


def test_delete_text(client: TestClient, db_session: Session, populate_words):
    _ = populate_words()
    rv = client.delete('/texts/increase')
    assert rv.status_code == 200

    db_text: Text = db_session.query(Text).filter(Text.text_value == 'increase').one()
    assert db_text.deleted is True

    rv = client.delete('/texts/unexisting_word')
    assert rv.status_code == 200


def test_populate(client, populate_words):
    populate_words()
