import uuid
from typing import Callable, Generator

import pytest
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session, joinedload

from app import create_app
from app.database import (
    Definition,
    Example,
    Status,
    Text,
    UnscopedSessionContext,
    db,
)


@pytest.fixture(scope='session', autouse=True)
def db_session() -> Generator[Session, None, None]:
    db.drop_all()
    db.create_all()
    with UnscopedSessionContext(db=db) as session:
        yield session
        db.drop_all()


@pytest.fixture(scope='module')
def client() -> Generator[TestClient, None, None]:
    with TestClient(app=create_app()) as c:
        yield c


DATA = [
    {
        'text_value': 'increase',
        'language': 'en',
        'translations': [
            {'text_value': 'увеличение', 'language': 'ru'},
            {'text_value': 'расти', 'language': 'ru'},
            {'text_value': 'Zunahme', 'language': 'de'},
            {'text_value': 'Anstieg', 'language': 'de'},
        ],
        'definitions': [
            {
                'text_value': 'an instance of growing or making greater',
                'kind': 'noun',
            },
            {
                'text_value': 'become or make greater in size, amount, intensity, or degree',
                'kind': 'verb',
            },
        ],
        'examples': [
            {
                'text_value': 'an increase from sixteen to eighteen clubs',
                'kind': 'noun',
            },
            {
                'text_value': 'we are aiming to increase awareness of social issues',
                'kind': 'verb',
            },
        ],
        'synonyms': ['grow', 'rise', 'get bigger'],
    },
    {
        'text_value': 'challenge',
        'language': 'en',
        'translations': [
            {'text_value': 'вызов', 'language': 'ru'},
            {'text_value': 'бросать вызов', 'language': 'ru'},
            {'text_value': 'herausforderung', 'language': 'de'},
            {'text_value': 'herausfordern', 'language': 'de'},
        ],
        'definitions': [
            {
                'text_value': 'a call to take part in a contest or competition',
                'kind': 'noun',
            },
            {
                'text_value': 'invite (someone) to engage in a contest',
                'kind': 'verb',
            },
        ],
        'examples': [
            {
                'text_value': 'a challenge to the legality of the order',
                'kind': 'noun',
            },
            {
                'text_value': 'a world title challenge',
                'kind': 'noun',
            },
        ],
        'synonyms': ['dare', 'provocation', 'summons'],
    },
]


@pytest.fixture(scope='function')
def populate_words(
    db_session: Session,
) -> Generator[Callable[[], list[uuid.UUID]], None, None]:
    all_text_ids: list[uuid.UUID] = []
    all_definition_ids: list[uuid.UUID] = []
    all_example_ids: list[uuid.UUID] = []

    def _populate_words() -> list[uuid.UUID]:
        for item in DATA:
            source_text = Text(
                id=uuid.uuid4(),
                text_value=item['text_value'],
                language=item['language'],
                status=Status.DONE,
            )
            all_text_ids.append(source_text.id)
            db_session.add(source_text)

            translations = [
                Text(
                    id=uuid.uuid4(),
                    text_value=value['text_value'],  # type: ignore
                    language=value['language'],  # type: ignore
                )
                for value in item['translations']
            ]
            source_text.translations.extend(translations)
            all_text_ids.extend(text.id for text in translations)

            definitions = [
                Definition(
                    id=uuid.uuid4(),
                    text_value=value['text_value'],  # type: ignore
                    kind=value['kind'],  # type: ignore
                    text_id=source_text.id,
                )
                for value in item['definitions']
            ]
            source_text.definitions = definitions
            all_definition_ids.extend(d.id for d in definitions)

            examples = [
                Example(
                    id=uuid.uuid4(),
                    text_value=value['text_value'],  # type: ignore
                    kind=value['kind'],  # type: ignore
                )
                for value in item['examples']
            ]
            source_text.examples = examples
            all_example_ids.extend(d.id for d in examples)

            synonyms = [
                Text(
                    id=uuid.uuid4(),
                    text_value=value,
                    language=source_text.language,
                )
                for value in item['synonyms']
            ]
            source_text.synonyms = synonyms
            all_text_ids.extend(text.id for text in synonyms)

        db_session.commit()
        db_session.close()

        return all_text_ids

    yield _populate_words
    _clean_database(db_session)


@pytest.fixture(scope='function')
def clean_database(db_session) -> Generator[None, None, None]:
    yield
    _clean_database(db_session)


def _clean_database(db_session) -> None:
    db_session.query(Definition).delete()
    db_session.query(Example).delete()

    words = (
        db_session.query(Text)
        .options(joinedload(Text.translations))
        .options(joinedload(Text.synonyms))
        .options(joinedload(Text.examples))
        .options(joinedload(Text.definitions))
        .all()
    )

    for word in words:
        word.translations = []
        word.synonyms = []
        db_session.delete(word)
    db_session.commit()
    db_session.close()
