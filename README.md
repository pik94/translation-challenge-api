# Translation Challenge API

A simple microservice to translate short texts.

## Table of Contents

- [Project Description](#project-description)
- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [Configuration](#configuration)
- [Implementation details](#implementation-details)


## Project Description

The Translation Challenge API is a microservice that provides an API for working with word definitions/translations 
using data from Google Translate. It allows users to translate short texts and provides additional information 
such as definitions, synonyms, and examples. The service can be extended to support more advanced translation 
features in the future.

## Features

- Get details about a specific text:
  - Endpoint: `/texts/challenge?source_lang=<source_land>&target_lang=<target_lang>`
  - The response includes definitions, synonyms, translations, and examples.

- Get the list of the words stored in the database. Pagination, sorting and filtering by word are supported. 
  - Endpoint: `/texts`
  - Supports pagination, sorting, and filtering by word.
  - Partial match by a word is also possible.
  - Definitions, synonyms, and translations are not included in the response by default, but can be enabled using query parameters.
  - 
- Delete a word from the database.
- Complete docs are available on `/docs`.

## Installation

### Build
To build the project and Docker images for the development stack, run the following command:
```bash
make -j
```

### Development
For development purposes, you can bring up the development stack by running:
```bash
make run-dev-stack
```

This will start the necessary containers for the development environment. 
You can then trigger endpoints using tools like `curl`, for example:
```bash
curl -X GET -H "Content-Type: application/json" "http://localhost:5000/texts/challenge?source_lang=en&target_lang=ru"
```

By default, make run-dev-stack does not rebuild the development stack. 
If you want to rebuild it, use the following command:
```bash
make run-dev-stack-rebuild
```


### Testing
To run all tests, execute the following command:
```bash
make run-tests
```
This command runs the tests inside a separate Docker container. 
If you want to run the tests on your local machine, use the following command:
```bash
make run-tests-local
```


### Production
The production deployment can be done automatically using GitLab CI. 
However, the project includes templates for the build, deploy, and health check stages of the CI pipeline, along with descriptions of how they can be performed.

The general idea is to build new Docker images and push them to a registry like Docker Hub or Google Cloud Registry. 
During the deployment stage, these images are downloaded onto the server, 
and docker-compose is used to run the production stack. 
Finally, a post-deploy stage checks the status by requesting a health check endpoint.


## Configuration

By default, the API accepts texts with a maximum length of 32 characters. 
You can modify this value by setting a different value for the MAX_TEXT_LENGTH_ALLOWED environment variable.

## Implementation details

### Common algorithm
The microservice operates in the following way:

1. The given text, source, and target language are checked in the local database.
2. If data such as translations, definitions, synonyms, and examples exist, a result is returned to the user.
3. Otherwise, there are two cases when the Google Translation API needs to be requested:
   1. The given text does not exist in the local database.
   2. The given text is in the database, but its data is incomplete. To check completeness and incompleteness statuses,
there are internal statuses "done" and "need_to_update", respectively.
This can happen if the word is part of a set of translations or synonyms of another text.
For example, if there is complete information for the word "increase" among its synonyms, and the word "grow" is
requested for translation with a status "need_to_update", it indicates that "grow" does not have translations,
definitions, examples, and synonyms yet. In this case, the Google Translation API is requested to enrich the data.
After that, the data for "grow" becomes complete.


The microservice the next way:
The given text, source and target language are checked in the local database. 
If data such as translations, definitions, synonyms and examples exist, a result is returned to a user. 
Otherwise, there are two cases when the Google Translation API has to be requested:
- the given text does not exist in the local database.
- the given text is in the database but its data incomplete. For checking completeness and incompleteness statuses, 
  there are internal statuses "done" and "need_to_update", respectively.
  It may happen because the word is a part of set of translations or synonyms of another text. 
  For example, if there are complete information for the word "increase" among its synonym there is a word "grow". If 
  "grow" is requested to translate and has a status "need_to_update", it indicates that it does not have translations, 
  definitions, examples and synonyms yet. So, the Google Translation API is requested to enrich data. After that, 
  data for "grow" becomes complete. 

**IMPORTANT**: The microservice API does not always have examples, definitions, and synonyms because the API accepts
arbitrary text.

To decrease latency for user responses, it might be worth adding a scheduled background process that checks texts 
with a `need_to_update` status and updates them. I see two possible options here: Celery or Apache Airflow.

### API
The API endpoints are implemented using FastAPI.

### Database
I have decided to use a single instance of PostgreSQL initially. While it may be suitable for the first time, for high load
it is worth considering multiple instances, such as one writing node and multiple read nodes. Alternatively, a NoSQL database
like MongoDB could be used, as it can be easily scaled.

All texts, translations, and synonyms are stored in a table `texts` and are linked to each other
via a many-to-many relationship. At first glance, this model resembles a graph and allows for efficient storage.
Definitions and examples are stored in separate tables and are linked to texts via a many-to-one relationship.

### Cache
I have also added a Redis node for caching data. Currently, caching is implemented by setting a two-minute cache duration.
For production, it would be beneficial to implement smarter caching strategies, such as considering text popularity based on
the number of user requests for a particular text or word.